// region Mask Functions

var Mask = {
	mask: function(_field, _function, _genericParameters) {

		if (_genericParameters) {
			_field.value = _function(_field.value, _genericParameters);
		} else {
			_field.value = _function(_field.value);
		}

	},

	generic: function(v, _genericParameters) {

		if (v) {
			var _regex = _genericParameters.regex;
			var _syntax = _genericParameters.syntax;
			var _maxValue = _genericParameters.maxValue;
			v = v.replace(/D/g, "");
			v = v.replace(_regex, _syntax);

			return (_maxValue != null) ? v.slice(0, _maxValue) : v;
		}

	},

	postcode: function(v) {

		if (v) {
			v = v.replace(/D/g, "");
			v = v.replace(/^(\d{5})(\d)/, "$1-$2");
			return v.slice(0, 9);
		}

	},

	phone: function(v) {

		if (v) {
			v = v.replace(/\D/g, "");
			v = v.replace(/^(\d\d\d)(\d)/g, "($1) $2");
			v = v.replace(/(\d{3})(\d)/, "$1-$2");
			return v.slice(0, 14);
		}

	}
};

// endregion

// region View Functions

// Clear Field
function _clearField() {

	$.textField.setValue('');

}

// Get Field Value
function _getValue() {

	return {
		fieldName: $.mainWidgetView.fieldId,
		value: $.textField.getValue()
	};

}

// Get Is Field Empty
function _isFieldEmpty() {

	return !!($.textField.getValue() == undefined || $.textField.getValue() == '');

}

// Set Background Color
function _setBackgroundColor(color) {

	$.textFieldParentView.setBackgroundColor(color);

}

// Set Border Color
function _setBorderColor(color) {

	$.textFieldParentView.setBorderColor(color);

}

// Set Field Value
function _setValue(value) {

	$.textField.setValue(value);

	if (value && value.length > 0) {
		$.hintText.setVisible(false);
	} else {
		$.hintText.setVisible(true);
	}

}

// Set Hint Text
function _setHintText(value) {

	$.hintText.setText(value);

}

// Set Hint Text Color
function _setHintTextColor(value) {

	$.hintText.setColor(value);

}

// Update Hint Text
function updateHintText(e) {

	$.hintText.setVisible(!e.source.value.length > 0);

}

// Validate
function _validate(defaultBorderColor, invalidBorderColor) {

	if (_isFieldEmpty()) {
		if (invalidBorderColor) {
			_setBorderColor(invalidBorderColor);
		}
		return false;
	} else {
		if (defaultBorderColor) {
			_setBorderColor(defaultBorderColor);
		}
		return true;
	}

}

// Initialize Widget
(function init() {

	args = $.args;

	// Add Methods
	$.mainWidgetView.clearField = _clearField;
	$.mainWidgetView.getValue = _getValue;
	$.mainWidgetView.setValue = _setValue;
	$.mainWidgetView.validate = _validate;

	// Remove Underline From TextField In Android
	if (OS_ANDROID) {
		$.textField.setBackgroundColor('transparent');
	}

	// Set Auto Capitalization
	$.textField.setAutocapitalization(args.autocapitalization);

	// Set Auto Correct
	$.textField.setAutocorrect(args.autcorrect);

	// Set Background Color
	$.textFieldParentView.setBackgroundColor(args.backgroundColor || 'white');

	// Set Border Color
	$.textFieldParentView.setBorderColor(args.borderColor || 'transparent');

	// Set Border Radius
	$.textFieldParentView.setBorderRadius(args.borderRadius || 0);

	// Set Field ID
	$.mainWidgetView.fieldId = args.id;

	// Set Font Color
	$.textField.setColor(args.fontColor || 'black');

	// Set Hint Text
	$.hintText.setText(args.hintText || '');

	// Set Hint Text Color
	$.hintText.setColor(args.hintTextColor || 'black');

	// Set Keyboard Type
	$.textField.setKeyboardType(args.keyboardType);

	// Set Opacity
	if (args.opacity) {
		$.textFieldParentView.setBackgroundColor('transparent');
		$.transparencyView.setBackgroundColor(args.backgroundColor || 'white');
		$.transparencyView.setOpacity(args.opacity);
	}

	// Set Password Mask
	$.textField.setPasswordMask(args.passwordMask || false);

	// Set Padding
	$.textField.setLeft(args.paddingLeft || 10);
	$.textField.setRight(args.paddingRight || 10);
	$.hintText.setLeft(args.paddingLeft || 10);
	$.hintText.setRight(args.paddingRight || 10);

	if (OS_ANDROID) {
		$.hintText.setLeft((args.paddingLeft + 3) || 13);
		$.hintText.setRight((args.paddingRight + 3) || 13);
	}

	// Set Phone Mask
	if (args.phoneMask) {
		$.textField.setKeyboardType(Titanium.UI.KEYBOARD_TYPE_NUMBER_PAD);
		$.textField.addEventListener('blur', function(e) {
			Mask.mask(e.source, Mask.phone);
		});
	}

	// Set Position And Dimensions
	$.mainWidgetView.setHeight(args.height || 50);
	$.mainWidgetView.setWidth(args.width || 'auto');
	$.mainWidgetView.setLeft(args.left || 0);
	$.mainWidgetView.setRight(args.right || 0);
	$.mainWidgetView.setTop(args.top || 0);
	$.mainWidgetView.setBottom(args.bottom || 0);

	// Set Required
	$.mainWidgetView.required = args.required || false;

	// Set Value
	$.textField.setValue(args.value);

	// Show/Hide Hint Text
	if (args.value && args.value.length > 0) {
		$.hintText.setVisible(false);
	} else {
		$.hintText.setVisible(true);
	}

}());

// endregion

// region Export Functions

exports = {

	// Add Text Field Event Listener
	addEventListener: $.textField.addEventListener,

	// Blur Text Field
	blurField: function() {

		$textField.blur();

	},

	// Clear Field
	clearField: _clearField,

	// Get Field Value
	getValue: _getValue,

	// Get Is Field Empty
	isFieldEmpty: _isFieldEmpty,

	// Set Background Color
	setBackgroundColor: _setBackgroundColor,

	// Set Border Color
	setBorderColor: _setBorderColor,

	// Set Field Value
	setValue: _setValue,

	// Set Hint Text
	setHintText: _setHintText,

	// Set Hint Text Color
	setHintTextColor: _setHintTextColor,

	// Set Text Field Off Events
	off: function(eventName, callback) {

		return $.textField.removeEventListener(eventName, callback);

	},

	// Set Text Field On Events
	on: function(eventName, callback) {

		return $.textField.addEventListener(eventName, callback);

	}

};

// endregion
